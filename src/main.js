import Vue from 'vue'
import App from './App'

import home from './pages/movie/home.vue'
Vue.component('home',home)

import find from './pages/movie/find/find.vue'
Vue.component('find',find)

import cate from './pages/movie/cate/cate.vue'
Vue.component('cate',cate)

import my from './pages/movie/my/my.vue'
Vue.component('my',my)



import basics from './pages/basics/home.vue'
Vue.component('basics',basics)

import components from './pages/component/home.vue'
Vue.component('components',components)

import plugin from './pages/plugin/home.vue'
Vue.component('plugin',plugin)

import cuCustom from './colorui/components/cu-custom.vue'
Vue.component('cu-custom',cuCustom)
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()

 



